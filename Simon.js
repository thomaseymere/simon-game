$(document).ready(function() {
	var attempt = [], simon = [], level = 1, lives = 3, index = 0, flag = true, color = "", playerTurn = false, soundIndex;
	var slices = ["red", "blue", "yellow", "green"];
	var redSound = new Audio("http://www.chiptape.com/chiptape/sounds/short/wipeHi.wav");
	var blueSound = new Audio("http://www.chiptape.com/chiptape/sounds/short/boingShort.wav");
	var yellowSound = new Audio("http://www.chiptape.com/chiptape/sounds/short/dinkMenu.wav");
	var greenSound = new Audio("http://www.chiptape.com/chiptape/sounds/short/horn.wav");
	var sounds = [redSound, blueSound, yellowSound, greenSound];
	


	$("#level").html(level);																		//Ininitialise le niveau a 1
	$("#lives").html(lives);																		//Initialise le nb de vie a 3
		
	$(".slice").on("click", function() {
		if (playerTurn) {																					// Ajouter une case au tableau quand le joueur ne joue pas
			color = $(this).attr("id")
			attempt.push(color);																		// Ajoute case niveau
			$(this).fadeOut(150).fadeIn(150);
			soundIndex = slices.indexOf(color);
			sounds[soundIndex].play();

			if (attempt[index] !== simon[index]) {									// Si incorrect
				alert("Incorrect, try again.");
				patternBlink(simon);
				index = 0;
				attempt = [];
				//flag = true;
			}
			else {
				if (index === simon.length-1) {									// Si tout est correct
					playerTurn = false;																		// Quand montre patterne, le user ne peut pas cliquer
					attempt = [];																					// reset le tableau
					level++;																							// Niveau up
					$("#level").html(level);
					index = 0;																						// Reset
					color = slices[Math.floor(Math.random() * (3 + 1))];	// Ajoute un bloc patterne
					simon.push(color);
					patternBlink(simon);
				}
				else if (index !== simon.length-1) {						// Si c'est correcte mais pas la fin du patterne
					index++;																							// augmente l'index
					return;
				}
			}
		}
	});

	$("#start-reset").on("click", function() {									// QUand on click sur le button
		if ($(this).html() === "Commencer") {
			simon = [];																							// Assure que tableau vide
			$(this).html("Recommencer");																	

			color = slices[Math.floor(Math.random() * (3 + 1))];		// Ajouter une couleur
			simon.push(color);

			patternBlink(simon);																		// appeller patterBlink() pour faire le premier patterne
		}
		else {																										// recommencer le jeu 
			reset();
		}
	});																													

	function patternBlink(simon) {
		setTimeout(function() {																		// Ajoute un delai du prochain clignotement de la couleur (double clignotement) 
			for(var i = 0; i < simon.length; i++) {									// Faire chaque clignement par segment
				(function(i) { 
					setTimeout(function() {
						soundIndex = slices.indexOf(simon[i]);
						sounds[soundIndex].play();
						$("#"+simon[i]).fadeOut(150).fadeIn(150);					// Faire selectionner la part de clignotement
					}, i * 800);
				})(i);																								// Tour de l'utilisateur													
			}		
		}, 1000);
		playerTurn = true;
	}

	function reset() {
		$("#start-reset").html("Commencer");
		level = 1;
		$("#level").html(level);
		//lives = 3;
		attempt = [];
		simon = [];
		index = 0;
	}
});																														
